Bievenido al repositorio online de mecánica de fluidos computacional ([CFD](http://es.wikipedia.org/wiki/Mec%C3%A1nica_de_fluidos_computacional))

Estos notebooks forman parte del primer módulo interactivo online de [CFD con Python](https://bitbucket.org/cfdpython/cfd-python-class) impartido por la profesora [Lorena A. Barba](http://lorenabarba.com/) y llamado **12 Steps to Navier-Stokes.**.

Texto y código sujeto bajo Creative Commons Attribution license, CC-BY-SA. (c) Original por Lorena A. Barba y Gilbert Forsyth en 2013, traducido por F.J. Navarro-Brull para [CAChemE.org](http://www.cacheme.org/)

[@LorenaABarba](https://twitter.com/LorenaABarba) - 
[@CAChemEorg](https://twitter.com/cachemeorg)



Lecciones
-------

* [Introduccion rapida a Python](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/00%2520-%2520Introduccion%2520rapida%2520a%2520Python.ipynb)
* [Paso 1](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/01%2520-%2520Paso%25201.ipynb)
* [Paso 2](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/02%2520-%2520Paso%25202.ipynb)
* [CFL Condition](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/03%2520-%2520CFL%2520Condition.ipynb)
* [Paso 3](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/04%2520-%2520Paso%25203.ipynb)
* [Paso 4](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/05%2520-%2520Paso%25204.ipynb)
* [Operaciones matriciales (arrays) con NumPy](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/06%2520-%2520Operaciones%2520matriciales%2520%2528arrays%2529%2520con%2520NumPy.ipynb)
* [Paso 5](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/07%2520-%2520Paso%25205.ipynb)
* [Paso 6](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/08%2520-%2520Paso%25206.ipynb)
* [Paso 7](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/09%2520-%2520Paso%25207.ipynb)
* [Paso 8](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/10%2520-%2520Paso%25208.ipynb)
* [Definiendo funciones](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/11%2520-%2520Definiendo%2520funciones.ipynb)
* [Paso 9](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/12%2520-%2520Paso%25209.ipynb)
* [Paso 10](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/13%2520-%2520Paso%252010.ipynb)
* [Optimizando bucles con Numba](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/14%2520-%2520Optimizando%2520bucles%2520con%2520Numba.ipynb)
* [Paso 11](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/15%2520-%2520Paso%252011.ipynb)
* [Paso 12](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/16%2520-%2520Paso%252012.ipynb)
